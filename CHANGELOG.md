# 0.3.0

Add an option to disable swapping if you only want to reverse axes (fixes #1)

# 0.2.2

Changes necessary for publishing to PyPI

# 0.2.1

Remove print statements

# 0.2.0

Add an option to reverse the X and/or Y axis

# 0.1.0

Internal testing release
